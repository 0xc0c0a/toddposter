require('dotenv').config();

const discord = require('discord.js');
const cron = require('node-cron');

const Identity = require('./identity');
const reactToMessage = require('./react-to-message');

(async () => {

  const client = new discord.Client();

  await Promise.all([
    new Promise((resolve, reject) => client.on('ready', resolve)),
    client.login(process.env.DISCORD_TOKEN)
  ]);

  const identity = new Identity(client, './data/identity.json');

  client.on('message', async message => reactToMessage(message, client, identity));

})();
