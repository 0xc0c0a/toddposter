const fs = require('fs');
const util = require('util');

module.exports = (() => {

  async function changeNickname(nickname) {

    /* I use guild nicknames instead of the username to get around rate limits.
     * The bot still has a global identity. */

    return Promise.all(
      this.client
        .guilds
        .array()
        .filter(guild => guild.available) // Nickname and avatar/identity may not match sometimes.
        .map(guild => guild.me.setNickname(nickname))
    );

  }

  async function changeAvatar(avatar) {
    return this.client.user.setAvatar(avatar);
  }

  function loadData(path) {

    try {
      return JSON.parse(fs.readFileSync(path));
    } catch (error) {
      return {};
    }

  }

  async function saveData(data, path) {
    await util.promisify(fs.writeFile)(path, JSON.stringify(data));
  }

  return class {

    constructor(client, dataPath) {
      this.client = client;

      this.dataPath = dataPath;
      this.data = loadData(dataPath);
    }

    async change(data) {

      /* No per-guild avatars means that Todd needs to be who he is on every
       * server.
       * So, nothing fancy here: set him for everyone, and do it synchronously,
       * letting the avatar go first, because that's what tends to fail. */

      if (this.data.avatar !== data.avatar) {
        await changeAvatar.bind(this)(data.avatar);
      }

      if (this.data.nickname !== data.nickname) {
        await changeNickname.bind(this)(data.nickname);
      }

      /* Only when nickname and avatar are set correctly do we *become* the new
       * identity. */

      this.data = data;
      await saveData(data, this.dataPath);

    }

  }

})();
