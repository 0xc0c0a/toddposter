const typeNaturally = require('./type-naturally');

module.exports = async (channel, {name, url, pitch}) => {

  for (const line of pitch) {
    await typeNaturally(channel, `_"${line}"_`);
  }

  await typeNaturally(channel, `**${name.toUpperCase()}**`);
  await typeNaturally(channel, '_Buy it now!_');
  await typeNaturally(channel, url);

}
