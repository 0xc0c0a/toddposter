const identities = require('./identities');
const products = require('./products');
const shill = require('./shill');
const typeNaturally = require('./type-naturally');

module.exports = async (message, client, identity) => {

  /* Todd shouldn't react to Todd. That would be unhealthy. */

  if (message.author === client.user) {
    return;
  }

  /* TODO: Limit Todd to doing one thing at once.
   * Maybe on Identity? Could I just change this to Todd?
   * It's the Todd-state-thing, after all. */

  const channel = message.channel;

  const reactions = [

    {
      regExp: /red\s+dead/i,
      callback: async () => {

        try {
          await identity.change(identities.toddMcHoward);
        } catch (error) {
          console.error(error);
          return;
        }

        const lines = [
          `Howdy partner. My name is Todd. Todd McHoward.`,
          `I heard ya and the other yokels were playin' that whacky "cowboy" game.`,
          `I am gonna tell ya that it really isn't worth your time.`,
          `Honestly, those tenderfoots need to learn how to make a proper video game.`,
          `I will rough up them half-wits and put them in calaboose, while you go play a real game like Fallout 76.`,
          `I've been there where it was made ya know.`,
          `So the jig is up and I better not find ya slacking around or ya are going to join city-slickers in the hoosegow.`,
          `Pony up, partner, or ya're a goner.`
        ];

        for (const line of lines) {
          await typeNaturally(channel, line);
        }

        await shill(channel, products.fallout76);

      }
    },

    {
      regExp: /nintendo|mario/i,
      callback: async () => {

        try {
          await identity.change(identities.superMario);
        } catch (error) {
          console.error(error);
          return;
        }

        const lines = [
          `Wahoo!`,
          `What a fantastic E3 victory by Nintendo, isn't that right, fellow Nintendo gamers?`,
          `Mario and Xeno-whatever look like good games I guess, but y'know what'll be even better than those?`,
          `Skyrim for the Nintendo Switch, that's set to come out this fall for only $59.99.`,
          `Be sure to pre-order if you already haven't!`,
          `And, a wahoo! to you all!`
        ];

        for (const line of lines) {
          await typeNaturally(channel, line);
        }
        await shill(channel, products.skyrimSwitch);

      }
    },

    {
      regExp: /skyrim/i,
      callback: async () => {

        const lines = [
          `Geez, oh man.`,
          `Is this about one of those "visual games" of his?`,
          `That boy used to talk about nothing else when he was a wee lad.`,
          `The other children used to make fun of him for it, called him a "dork".`,
          `Then again, he never quite was the athletic type.`,
          `President of the chess club, though.`,
          `No matter what, I am proud of my boy, and y'all should very well consider buying that "Skyer" game of his.`
        ];

        const lines = [
          `It has come to my attention that some of you were making fun of my little Toddy.`,
          `This is unacceptable.`,
          `Buy his game right now or I will have a word with your parents and inform your school principal.`,
          `My little sunshine is sitting next to me right now.`,
          `Crying.`,
          `It took him so much courage to confess this to me.`,
          `You _WILL_ buy Skyrim for the Switch and you WILL buy Fallout 4 VR.`,
          `Do I make myself clear, young man?`
        ];

      }
    }

  ];

  const react =
    reactions
      .map( ({ regExp, callback }) => {
        const result = regExp.exec(message.content);
        if (result == null) {
          return null;
        }
        return { index: result.index, callback };
      })
      .filter(reaction => reaction !== null)
      .reduce(
        (currentEarliestMatch, currentMatch) => (currentMatch.index < currentEarliestMatch.index) ? currentMatch : currentEarliestMatch,
        { index: Infinity, callback: () => {} } // No-op
      )
      .callback;

  return react();

}
