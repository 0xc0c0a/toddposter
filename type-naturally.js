module.exports = async (channel, line) => {

  const wordsPerMinute = 160;
  const typingDuration = 1000 * 60 * (line.split(' ').length/wordsPerMinute);

  channel.startTyping();

  await new Promise(resolve => setTimeout(resolve, typingDuration));
  await channel.send(line);

  channel.stopTyping();

}
